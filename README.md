# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm_Project_1092083s

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://chatroom-e13ac.web.app

## Website Detail Description

### 主要功能：
會員登入
![](https://i.imgur.com/1wZ3C97.png)

會員註冊
![](https://i.imgur.com/hqivEtJ.png)

會員資料及登出鈕
![](https://i.imgur.com/DTEqtoG.png)

聊天大廳
![](https://i.imgur.com/heuq798.png)

創建群組聊天室
![](https://i.imgur.com/vetX6Gm.png)

新增聊天室成員
![](https://i.imgur.com/yCKoP5X.png)

傳送圖片中
![](https://i.imgur.com/T3Qo1GP.png)

群組聊天狀況
![](https://i.imgur.com/99fMiGU.png)

---
### 以下是其他功能：

用戶資料修改
![](https://i.imgur.com/H6F9JHG.png)

私訊聊天
![](https://i.imgur.com/HUqitY5.png)




# Components Description : 
1. 可以正常登入，且創立私人群組、私人聊天室
2. 傳送圖片 : 點擊圖片標示可以傳送圖片至聊天室，如要跟文字一起發送可先打文字再選圖片，並加上等待中的狀態顯示。
3. 群組聊天 : 創建群組，可以透過email加入群組成員


# Other Functions Description : 
1. 用戶資料更新 : 用戶可以點擊右上角，來到用戶資料頁面，可自行更換頭貼或是暱稱。
2. 單獨私聊 : 在群組聊天室內可以直接點擊他人頭像，創造一對一聊天室。