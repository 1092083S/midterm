import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {},
    currentChatroom: {
      roomName: '聊天大廳',
      roomId: 'lobby',
      roomMembers: [],
      roomType: 'group',
    },
    chatroomList: [],
    msgUserInfo: {},
    isLoading: false,
    sidebarActive: true,
    loginTime: '',
  },
  mutations: {
    updateLoginTime(state, loginTime) {
      state.loginTime = loginTime;
    },
    updateCurrentChatroom(state, object) {
      state.currentChatroom = object;
    },
    updateUser(state, user) {
      state.user = user;
    },
    updateChatroomList(state, array) {
      state.chatroomList = array;
    },
    updateMsgUserInfo(state, msgUserInfo) {
      state.msgUserInfo = msgUserInfo;
    },
    updateIsLoading(state, boolean) {
      state.isLoading = boolean;
    },
    updateSidebarActive(state, boolean) {
      state.sidebarActive = boolean;
    },
  },
  actions: {
  },
  modules: {
  },
});
