import firebase from 'firebase';
import router from '@/router';
import store from '@/store';

const config = {
  apiKey: 'AIzaSyDhYNl7TMef35Cszj7CTWbb7ZWyjtnpYwc',
  authDomain: 'chatroom-e13ac.firebaseapp.com',
  databaseURL: 'https://chatroom-e13ac-default-rtdb.firebaseio.com/',
  projectId: 'chatroom-e13ac',
  storageBucket: 'chatroom-e13ac.appspot.com',
  messagingSenderId: '390369819981',
  appId: '1:390369819981:web:689f3a4483c39df35e8b04',
  measurementId: 'G-R143WWP0HG',
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    console.log('user:', user);
    store.commit('updateLoginTime', Date.now());

    const db = firebase.firestore();

    const docRef = db.collection('users').doc(user.uid);
    docRef.get().then((doc) => {
      if (doc.exists) {
        console.log('Document data:', doc.data());
        console.log('非第一次登入');

        const msgRoomRef = '/chatroom';
        firebase.database().ref(msgRoomRef).once('value')
          .then((snapshot) => {
            const allChatroom = snapshot.val();
            const chatroomIdList = doc.data().userChatroomList;
            const chatroomList = [];
            chatroomIdList.forEach((chatroomId) => {
              chatroomList.push(allChatroom[chatroomId].Info);
            });
            store.commit('updateChatroomList', chatroomList);
          })
          .catch((e) => console.log(e.message));
      } else {
        const usersRef = db.collection('users');
        if (!user.displayName) {
          // eslint-disable-next-line no-param-reassign
          user.displayName = user.email;
        }
        usersRef.doc(user.uid).set({
          userInfo: JSON.parse(JSON.stringify(user)),
          userChatroomList: ['lobby'],
        });
        const roomMembersRef = '/chatroom/lobby/Info/roomMembers';
        firebase.database().ref(roomMembersRef).push(user.uid);
      }
      store.commit('updateUser', user);
    }).catch((error) => {
      console.log('Error getting document:', error);
    });

    router.push('/userHomePage');
    console.log('登入成功');
  } else {
    router.push('/');
    console.log('已登出');
    // It won't show any post if not login
  }
});
