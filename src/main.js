import '@babel/polyfill';
import 'mutationobserver-shim';
import Vue from 'vue';
import vmodal from 'vue-js-modal';
import './plugins/bootstrap-vue';
import Loading from 'vue-loading-overlay';
import Notifications from 'vue-notification';
import VueNativeNotification from 'vue-native-notification';
import App from './App.vue';
import router from './router';
import store from './store';
import 'vue-loading-overlay/dist/vue-loading.css';
import 'bootstrap/dist/css/bootstrap.css';
import '@/libs/config';

Vue.config.productionTip = false;
Vue.use(vmodal);
Vue.use(Notifications);
Vue.use(VueNativeNotification, {
  requestOnNotify: true,
});
Vue.component('Loading', Loading);

const main = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

export default main;
