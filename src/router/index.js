import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'LoginPage',
    component: () => import('../views/LoginPage.vue'),
  },
  {
    path: '/userHomePage',
    name: 'UserHomePage',
    component: () => import('../views/UserHomePage.vue'),
    children: [
      {
        path: '',
        name: 'Chatroom',
        component: () => import('@/components/ChatRoom.vue'),
      },
      {
        path: 'personalInfo',
        name: 'PersonalInfo',
        component: () => import('@/components/PersonalInfo.vue'),
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return new Promise((resolve, reject) => {
    originalPush.call(this, location, () => {
      resolve(this.currentRoute);
    }, (error) => {
      if (error.name === 'NavigationDuplicated') {
        resolve(this.currentRoute);
      } else {
        reject(error);
      }
    });
  });
};

export default router;
